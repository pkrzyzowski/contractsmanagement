﻿namespace ContractsManagement.DTO
{
    public class ContractType
    {
        public int ContractTypeId { get; set; }
        public string ContractTypeName { get; set; }
    }
}
