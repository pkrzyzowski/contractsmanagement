﻿namespace ContractsManagement.DTO
{
    public class Contract
    {
        public int? ContractId { get; set; }
        public int ContractTypeId { get; set; }
        public string ContractName { get; set; }
        public int YearsOfExperience { get; set; }
        public decimal? Salary { get; set; }
    }
}
