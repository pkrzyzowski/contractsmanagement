﻿using Moq;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ContractsManagement.Repository;
using ContractsManagement.Repository.Entities;
using System.Web.Http.Results;

namespace ContractsManagement.Api.UnitTests.Controllers
{
    [TestClass]
    public class ContractValidationControllerTests
    {
        private Mock<IContractsManagementRepository> mockedRepository;
        private Api.Controllers.ContractValidationController contractValidationController;
        private Fixture fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            this.mockedRepository = new Mock<IContractsManagementRepository>();
            this.contractValidationController = new Api.Controllers.ContractValidationController(this.mockedRepository.Object);
            this.fixture = new Fixture();
        }

        [TestMethod]
        public void ContractValidationControllerPostShouldReturn200WhenValidContractWasSent()
        {
            //Arrange:
            DTO.Contract contract = this.fixture.Create<DTO.Contract>();
            this.mockedRepository
                .Setup(p => p.CreateContract(It.IsAny<DTO.Contract>(), It.Is<bool>(v => v == true)))
                .Returns(new RepositoryStatus<Contract>(null, EntityStatus.Created, null));

            //Act:
            var result = this.contractValidationController.Post(contract);

            //Assert:
            Assert.IsInstanceOfType(result, typeof(OkResult), "Expected 200 status code.");
        }

        [TestMethod]
        public void ContractValidationControllerPostShouldReturn400WhenValidContractWasSent()
        {
            //Arrange:
            DTO.Contract contract = new DTO.Contract { ContractTypeId = 3 };
            this.mockedRepository
                .Setup(p => p.CreateContract(It.IsAny<DTO.Contract>(), It.Is<bool>(v => v == true)))
                .Returns(new RepositoryStatus<Contract>(null, EntityStatus.Created, new List<ValidationError>() { new ValidationError { FieldName = "Name", Message = "Test error" } }));

            //Act:
            var result = this.contractValidationController.Post(contract);

            //Assert:
            Assert.IsInstanceOfType(result, typeof(BadRequestErrorMessageResult), "Expected 400 status code.");
            List<ValidationError> validationErrors = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ValidationError>>(((BadRequestErrorMessageResult)result).Message);
            Assert.IsTrue(validationErrors.Any());
        }
    }
}
