﻿using Moq;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ContractsManagement.Repository;
using System.Web.Http.Results;
using System.Linq.Expressions;

namespace ContractsManagement.Api.UnitTests.Controllers
{
    [TestClass]
    public class SalaryControllerTests
    {
        private Mock<IContractsManagementRepository> mockedRepository;
        private Api.Controllers.SalaryController salaryController;
        private Fixture fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            this.mockedRepository = new Mock<IContractsManagementRepository>();
            this.salaryController = new Api.Controllers.SalaryController(this.mockedRepository.Object);
            this.fixture = new Fixture();
        }

        [TestMethod]
        public void SalaryControllerShouldCallCalculateSalaryOnGet()
        {
            //Arrange:
            int testType = 1;
            int testYearsOfExperience = 4;
            decimal testSalary = 5000;

            Expression<Func<int, bool>> testTypeMatch = v => v == testType;
            Expression<Func<int, bool>> testYearsOfExperienceMatch = v => v == testYearsOfExperience;

            mockedRepository.Setup(p => p.CalculateSalary(It.Is<int>(testTypeMatch), It.Is<int>(testYearsOfExperienceMatch))).Returns(testSalary);

            //Act:
            var result = salaryController.Get(testType, testYearsOfExperience);

            //Assert:
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<decimal>), "Expected 200 status code and IEnumerable<DTO.Contract> in response content.");
            mockedRepository.Verify(p=>p.CalculateSalary(It.Is<int>(testTypeMatch), It.Is<int>(testYearsOfExperienceMatch)),Times.Once);
            Assert.AreEqual(testSalary, ((OkNegotiatedContentResult<decimal>)result).Content);
        }

    }
}
