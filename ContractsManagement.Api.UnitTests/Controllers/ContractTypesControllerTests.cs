﻿using Moq;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ContractsManagement.Repository.Entities;
using ContractsManagement.Repository;
using System.Web.Http.Results;

namespace ContractsManagement.Api.UnitTests.Controllers
{
    [TestClass]
    public class ContractTypesControllerTests
    {
        private Mock<IContractsManagementRepository> mockedRepository;
        private Api.Controllers.ContractTypesController contractTypesController;
        private Fixture fixture;

        [TestInitialize]
        public void TestInitialize()
        {
            this.mockedRepository = new Mock<IContractsManagementRepository>();
            this.contractTypesController = new Api.Controllers.ContractTypesController(this.mockedRepository.Object);
            this.fixture = new Fixture();
        }

        [TestMethod]
        public void ContractTypesControllerGetShouldReturnListOfContractTypes()
        {
            //Arrange:
            this.mockedRepository.Setup(p => p.GetContractTypes()).Returns(
                new List<ContractType> {
                    new ContractType { ContractTypeId = 1, ContractTypeName = "Programmer" },
                    new ContractType { ContractTypeId = 2, ContractTypeName = "Tester" }
                }.AsQueryable()
            );

            //Act:
            var result = this.contractTypesController.Get();

            //Assert:
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IQueryable<DTO.ContractType>>), "Expected 200 status code.");
            OkNegotiatedContentResult<IQueryable<DTO.ContractType>> successResponse = (OkNegotiatedContentResult<IQueryable<DTO.ContractType>>)result;
            Assert.IsTrue(successResponse.Content.Any());
        }
    }
}
