﻿using Moq;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Http;
using System.Net.Http;
using System.Web.Http.Results;
using Ploeh.AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ContractsManagement.Repository;
using ContractsManagement.Repository.Entities;
using System.Collections.Generic;
using Microsoft.Owin;
using System.Web.Http.Routing;

namespace ContractsManagement.Api.UnitTests.Controllers
{
    [TestClass]
    public class ContractControllerTests
    {
        private const int TestPageSize = 10;
        private const string PaginationHeaderName = Headers.PaginationHeader.PaginationHeaderName;
        private const string AllContractsRouteName = "All Contracts";

        private Mock<IContractsManagementRepository> mockedRepository;
        private Api.Controllers.ContractController contractController;
        private Fixture fixture;
        private Repository.Factories.ContractFactory contractFactory;
        private Uri baseUri = new Uri("http://localhost/");
        private OwinContext owinContext;
        private UrlHelper urlHelper;

        private Contract testContract;
        private DTO.Contract testContractDTO;
        
        private bool AreContractsEqual(DTO.Contract expected, DTO.Contract actual)
        {
            return
                expected.ContractId == actual.ContractId
                && expected.ContractName == actual.ContractName
                && expected.ContractTypeId == actual.ContractTypeId
                && expected.Salary == actual.Salary
                && expected.YearsOfExperience == actual.YearsOfExperience;
        }

        private IQueryable<Contract> CreateListOfContracts(int numberOfItems)
        {
            return this.fixture.CreateMany<Contract>(numberOfItems).AsQueryable();
        }

        private Headers.PaginationHeader GetPaginationHeader()
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<Headers.PaginationHeader>(
                this.owinContext.Response.Context.Response.Headers.Get(PaginationHeaderName)
            );
        }

        private int GetPagesCount(int numberOfContracts)
        {
            return (int)Math.Ceiling((float)numberOfContracts / TestPageSize);
        }

        private void AssertGetOnCollectionResult(object result, IQueryable<Contract> expectedContracts, Headers.PaginationHeader expectedPaginationHeader = null)
        {
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<IEnumerable<DTO.Contract>>), "Expected 200 status code and IEnumerable<DTO.Contract> in response content.");
            OkNegotiatedContentResult<IEnumerable<DTO.Contract>> successResponse = (OkNegotiatedContentResult<IEnumerable<DTO.Contract>>)result;

            if(expectedPaginationHeader != null)
            {
                Assert.AreEqual(successResponse.Content.Count(), TestPageSize);
                Assert.AreEqual(expectedPaginationHeader, GetPaginationHeader());
            }

            Assert.AreEqual(expectedContracts.Count(), successResponse.Content.Count(), "The number of returned contracts is different than expected!");

            for (int i = 0; i < expectedContracts.Count(); i++)
            {
                AreContractsEqual(contractFactory.GetContract(expectedContracts.ElementAt(i)), successResponse.Content.ElementAt(i));
            }
        }

        private int CalculateExpectedSalary()
        {
            return new Random().Next(); // :-)
        }

        private void SetupCreateContractMethod(DTO.Contract contract, bool onlyValidation, RepositoryStatus<Contract> returningValue)
        {
            mockedRepository.Setup(
                p => p.CreateContract(
                    It.Is<DTO.Contract>(param => AreContractsEqual(contract, param)),
                    It.Is<bool>(param => param == onlyValidation)
                )
            ).Returns(
                returningValue
            );
        }

        private void AssertSingleContract(object result)
        {
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<DTO.Contract>), "Expected 200 status code and DTO.Contract in response content.");
            OkNegotiatedContentResult<DTO.Contract> successResponse = (OkNegotiatedContentResult<DTO.Contract>)result;
            AreContractsEqual(this.testContractDTO, successResponse.Content);
        }

        private Contract CreateContractFromDTO(DTO.Contract contract, int salary)
        {
            return new Contract
            {
                ContractName = contract.ContractName,
                ContractTypeId = contract.ContractTypeId,
                YearsOfExperience = contract.YearsOfExperience,
                Salary = salary
            };
        }

        private void SetupUpdateContract(int contractId, Contract expectedContract, EntityStatus returnedStatus)
        {
            this.mockedRepository.Setup(m => m.UpdateContract(It.Is<int>(p => p == contractId), It.Is<Contract>(p => p == expectedContract))).Returns(
                new RepositoryStatus<Contract>(expectedContract, returnedStatus)
            );
        }

        [TestInitialize]
        public void TestInitialize()
        {
            var config = WebApiConfig.RegisterRoutes(new HttpConfiguration());
            this.mockedRepository = new Mock<IContractsManagementRepository>();
            this.contractFactory = new Repository.Factories.ContractFactory();
            this.fixture = new Fixture();
            this.testContract = fixture.Create<Contract>();
            this.testContractDTO = fixture.Create<DTO.Contract>();
            this.owinContext = new OwinContext();
            
            this.contractController = new Api.Controllers.ContractController(mockedRepository.Object);
            contractController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = baseUri };
            contractController.Configuration = config;
            contractController.Request.SetOwinContext(this.owinContext);

            this.urlHelper = new UrlHelper(contractController.Request);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            this.contractController.Dispose();
        }

        [TestMethod]
        public void ContractControllerGetMethodOnSingleItemShouldReturnSingleContract()
        {
            //Arrange:
            mockedRepository.Setup(p => p.GetContract(It.Is<int>(param=>param == testContract.ContractId))).Returns(testContract);

            //Act:
            var result = contractController.Get(testContract.ContractId);

            //Assert
            Assert.IsInstanceOfType(result, typeof(OkNegotiatedContentResult<DTO.Contract>), "Expected 200 status code and DTO.Contract in response content.");
            OkNegotiatedContentResult<DTO.Contract> successResponse = (OkNegotiatedContentResult<DTO.Contract>)result;
            Assert.IsTrue(AreContractsEqual(this.contractFactory.GetContract(testContract), successResponse.Content));
        }

        [TestMethod]
        public void ContractControllerGetMethodOnSingleItemShouldReturn404()
        {
            //Arrange:
            mockedRepository.Setup(p => p.GetContract(It.Is<int>(param => param == testContract.ContractId))).Returns((Contract)null);

            //Act:
            var result = contractController.Get(testContract.ContractId);

            //Assert
            Assert.IsInstanceOfType(result, typeof(NotFoundResult), "Expected 404 status code.");
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionShouldReturnFirstPageOfContracts()
        {
            //Arrange:
            int numberOfContracts = new Random().Next(15,100);
            IQueryable<Contract> testContracts = CreateListOfContracts(numberOfContracts);
            mockedRepository.Setup(p => p.GetContracts()).Returns(testContracts);

            var expectedPaginationHeader = new Headers.PaginationHeader {
                CurrentPage = 1,
                PageSize = TestPageSize,
                PagesCount = GetPagesCount(numberOfContracts),
                PreviousPage = string.Empty,
                NextPage = this.urlHelper.Link(AllContractsRouteName, new { pageNumber = 2, pageSize = TestPageSize })
            };

            //Act:
            var result = contractController.Get(1, TestPageSize);

            //Assert:
            AssertGetOnCollectionResult(result, testContracts.Take(TestPageSize), expectedPaginationHeader);
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionShouldReturnPenultimatePageOfContracts()
        {
            //Arrange:
            int numberOfContracts = new Random().Next(15, 100);
            IQueryable<Contract> testContracts = CreateListOfContracts(numberOfContracts);
            mockedRepository.Setup(p => p.GetContracts()).Returns(testContracts);

            int pagesCount = GetPagesCount(numberOfContracts);

            var expectedPaginationHeader = new Headers.PaginationHeader
            {
                CurrentPage = pagesCount-1,
                PageSize = TestPageSize,
                PagesCount = pagesCount,
                PreviousPage = this.urlHelper.Link(AllContractsRouteName, new { pageNumber = pagesCount-2, pageSize = TestPageSize }),
                NextPage = this.urlHelper.Link(AllContractsRouteName, new { pageNumber = pagesCount, pageSize = TestPageSize })
            };

            //Act:
            var result = contractController.Get(pagesCount-1, TestPageSize);

            //Assert:
            AssertGetOnCollectionResult(result, testContracts.Skip((pagesCount-2)*TestPageSize).Take(TestPageSize), expectedPaginationHeader);
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionShouldReturnOnlyProgrammersWith5YearsOfExperience()
        {
            //Arrange:
            List<Contract> testContracts = new List<Contract>() {
                new Contract() { ContractId = 1, ContractName = "Test1", ContractTypeId = 1, Salary = 5034, YearsOfExperience = 5 },
                new Contract() { ContractId = 1, ContractName = "Test3", ContractTypeId = 2, Salary = 5034, YearsOfExperience = 6 },
                new Contract() { ContractId = 1, ContractName = "Test2", ContractTypeId = 1, Salary = 3034, YearsOfExperience = 4 },
                new Contract() { ContractId = 1, ContractName = "Test4", ContractTypeId = 2, Salary = 3034, YearsOfExperience = 3 }
            };

            testContracts.AddRange(testContracts);

            mockedRepository.Setup(p => p.GetContracts()).Returns(testContracts.AsQueryable()); 

            //Act:
            var result = contractController.Get(1,10,"experience,4;type,1");

            //Assert:
            AssertGetOnCollectionResult(result, testContracts.Where(p=>p.YearsOfExperience > 4 && p.ContractTypeId == 1).AsQueryable());
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionShouldReturnOnlyPeopleWithSalaryOver5000()
        {
            //Arrange:
            List<Contract> testContracts = new List<Contract>() {
                new Contract() { ContractId = 1, ContractName = "Test1", ContractTypeId = 1, Salary = 5034, YearsOfExperience = 5 },
                new Contract() { ContractId = 1, ContractName = "Test4", ContractTypeId = 2, Salary = 3034, YearsOfExperience = 3 }
            };

            testContracts.AddRange(testContracts);

            mockedRepository.Setup(p => p.GetContracts()).Returns(testContracts.AsQueryable());

            //Act:
            var result = contractController.Get(1, 10, "salary,5000");

            //Assert:
            AssertGetOnCollectionResult(result, testContracts.Where(p => p.Salary > 5000).AsQueryable());
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionShouldReturnCollectionFilteredByName()
        {
            //Arrange:
            List<Contract> testContracts = new List<Contract>() {
                new Contract() { ContractId = 1, ContractName = "Test1", ContractTypeId = 1, Salary = 5034, YearsOfExperience = 5 },
                new Contract() { ContractId = 1, ContractName = "Test4", ContractTypeId = 2, Salary = 3034, YearsOfExperience = 3 }
            };

            testContracts.AddRange(testContracts);

            mockedRepository.Setup(p => p.GetContracts()).Returns(testContracts.AsQueryable());

            //Act:
            var result = contractController.Get(1, 10, "name,4");

            //Assert:
            AssertGetOnCollectionResult(result, testContracts.Where(p => p.ContractName.Contains("4")).AsQueryable());
        }

        [TestMethod]
        public void ContractControllerGetMethodOnCollectionReturns404()
        {
            //Assert:
            mockedRepository.Setup(p => p.GetContracts()).Returns((IQueryable<Contract>)null);

            //Act:
            var result = contractController.Get();

            //Assert:
            Assert.IsInstanceOfType(result, typeof(NotFoundResult), "Expected 404 status code.");
        }

        [TestMethod]
        public void ContractControllerPostMethodShouldInsertNewContract()
        {
            //Arrange:
            Contract expectedContract = CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary());

            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    expectedContract,
                    EntityStatus.Created
                )
            );
            this.mockedRepository.Setup(m=>m.InsertContract(It.Is<Contract>( p=>p == expectedContract))).Returns(
                new RepositoryStatus<Contract>(expectedContract, EntityStatus.Created)
            );

            //Act:
            var result = this.contractController.Post(this.testContractDTO);

            //Assert:
            AssertSingleContract(result);
            mockedRepository.Verify(m => m.InsertContract(It.Is<Contract>(p=>p == expectedContract)), Times.Once, "IContractsManagementRepository.InsertContract was expected to be triggered at least once.");
        }

        [TestMethod]
        public void ContractControllerPostMethodShouldReturnBadRequestIfUnableToCreateNewContract()
        {
            //Arrange:
            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary()),
                    EntityStatus.Error
                )
            );

            //Act:
            var result = this.contractController.Post(this.testContractDTO);

            //Assert:
            Assert.IsInstanceOfType(result, typeof(BadRequestResult), "Expected 400 status code.");
        }

        [TestMethod]
        public void ContractControllerPutMethodShouldUpdateContract()
        {
            //Arrange:
            Contract expectedContract = CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary());
            int contractId = new Random().Next();

            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    expectedContract,
                    EntityStatus.Created
                )
            );

            SetupUpdateContract(contractId, expectedContract, EntityStatus.Updated);

            //Act:
            var result = this.contractController.Put(contractId, this.testContractDTO);

            //Arrange:
            AssertSingleContract(result);
            mockedRepository.Verify(m => m.UpdateContract(It.Is<int>(p=>p == contractId), It.Is<Contract>(p => p == expectedContract)), Times.Once, "IContractsManagementRepository.InsertContract was expected to be triggered at least once.");
        }

        [TestMethod]
        public void ContractControllerPutMethodShouldReturn404IfContractNotFound()
        {
            //Arrange:
            Contract expectedContract = CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary());
            int contractId = new Random().Next();

            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    expectedContract,
                    EntityStatus.Created
                )
            );

            SetupUpdateContract(contractId, expectedContract, EntityStatus.NotFound);

            //Act:
            var result = this.contractController.Put(contractId, this.testContractDTO);

            //Arrange:
            Assert.IsInstanceOfType(result, typeof(NotFoundResult), "Expected 404 status code.");
            mockedRepository.Verify(m => m.UpdateContract(It.Is<int>(p => p == contractId), It.Is<Contract>(p => p == expectedContract)), Times.Once, "IContractsManagementRepository.InsertContract was expected to be triggered at least once.");
        }

        [TestMethod]
        public void ContractControllerPutMethodShouldReturn500IfRepositoryErrorOccured()
        {
            //Arrange:
            Contract expectedContract = CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary());
            int contractId = new Random().Next();

            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    expectedContract,
                    EntityStatus.Created
                )
            );

            SetupUpdateContract(contractId, expectedContract, EntityStatus.Error);

            //Act:
            var result = this.contractController.Put(contractId, this.testContractDTO);

            //Arrange:
            Assert.IsInstanceOfType(result, typeof(InternalServerErrorResult), "Expected 500 status code.");
            mockedRepository.Verify(m => m.UpdateContract(It.Is<int>(p => p == contractId), It.Is<Contract>(p => p == expectedContract)), Times.Once, "IContractsManagementRepository.InsertContract was expected to be triggered at least once.");
        }

        [TestMethod]
        public void ContractControllerPutMethodShouldReturnNotModifiedIfNothingChanged()
        {
            //Arrange:
            Contract expectedContract = CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary());
            int contractId = new Random().Next();

            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    expectedContract,
                    EntityStatus.Created
                )
            );

            SetupUpdateContract(contractId, expectedContract, EntityStatus.NothingModified);

            //Act:
            var result = this.contractController.Put(contractId, this.testContractDTO);

            //Arrange:
            Assert.IsInstanceOfType(result, new StatusCodeResult(System.Net.HttpStatusCode.NotModified, this.contractController).GetType(), "Expected 304 status code.");
            mockedRepository.Verify(m => m.UpdateContract(It.Is<int>(p => p == contractId), It.Is<Contract>(p => p == expectedContract)), Times.Once, "IContractsManagementRepository.InsertContract was expected to be triggered at least once.");
        }

        [TestMethod]
        public void ContractControllerPutMethodShouldReturnBadRequestIfUnableToCreateNewContract()
        {
            //Arrange:
            SetupCreateContractMethod(
                this.testContractDTO,
                false,
                new RepositoryStatus<Contract>(
                    CreateContractFromDTO(this.testContractDTO, CalculateExpectedSalary()),
                    EntityStatus.Error
                )
            );

            //Act:
            var result = this.contractController.Put(1, this.testContractDTO);

            //Assert:
            Assert.IsInstanceOfType(result, typeof(BadRequestResult), "Expected 400 status code.");
        }
    }
}
