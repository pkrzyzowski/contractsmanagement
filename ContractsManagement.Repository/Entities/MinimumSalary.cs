﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Repository.Entities
{
    public class MinimumSalary
    {
        [Key, Column(Order=0)]
        public int ContractTypeId { get; set; }

        [Key, Column(Order=1)]
        public int LowerLimitOfExperience { get; set; }

        [Key, Column(Order=2)]
        public int UpperLimitOfExperience{ get; set; }
        
        public decimal Salary { get; set; }
    }
}
