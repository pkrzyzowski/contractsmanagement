﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Repository.Entities
{
    public class ContractType
    {
        [Key]
        public int ContractTypeId { get; set; }
        public string ContractTypeName { get; set; }
        public decimal MultiplyFactor { get; set; }

        public virtual ICollection<MinimumSalary> MinimumSalaries { get; set; }
    }
}
