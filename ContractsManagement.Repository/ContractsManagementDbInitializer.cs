﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository
{
    class ContractsManagementDbInitializer : DropCreateDatabaseIfModelChanges<ContractsManagementContext>
    {
        protected override void Seed(ContractsManagementContext context)
        {
            ContractType programmerContractType = context.ContractTypes.Add(new ContractType { ContractTypeName = ContractTypes.Programmer, MultiplyFactor = 125 });
            ContractType testerContractType = context.ContractTypes.Add(new ContractType { ContractTypeName = ContractTypes.Tester, MultiplyFactor = 100 });

            context.SaveChanges();

            List<MinimumSalary> defaultMinimumSalaries = new List<MinimumSalary>
            {
                new MinimumSalary { ContractTypeId = programmerContractType.ContractTypeId, LowerLimitOfExperience = 1, UpperLimitOfExperience = 2, Salary = 2500 },
                new MinimumSalary { ContractTypeId = programmerContractType.ContractTypeId, LowerLimitOfExperience = 3, UpperLimitOfExperience = 5, Salary = 5000 },
                new MinimumSalary { ContractTypeId = programmerContractType.ContractTypeId, LowerLimitOfExperience = 6, UpperLimitOfExperience = int.MaxValue, Salary = 5500 },
                new MinimumSalary { ContractTypeId = testerContractType.ContractTypeId, LowerLimitOfExperience = 1, UpperLimitOfExperience = 1, Salary = 2000 },
                new MinimumSalary { ContractTypeId = testerContractType.ContractTypeId, LowerLimitOfExperience = 2, UpperLimitOfExperience = 4, Salary = 2700 },
                new MinimumSalary { ContractTypeId = testerContractType.ContractTypeId, LowerLimitOfExperience = 5, UpperLimitOfExperience = int.MaxValue, Salary = 3200 }
            };

            context.MinimumSalaries.AddRange(defaultMinimumSalaries);

            base.Seed(context);
        }
    }
}
