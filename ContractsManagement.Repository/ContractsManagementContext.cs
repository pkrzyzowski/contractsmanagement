﻿using System.Collections.Generic;
using System.Data.Entity;

namespace ContractsManagement.Repository
{
    public class ContractsManagementContext : DbContext
    {
        public ContractsManagementContext() : base("name=ContractsManagementContext")
        {
            Database.SetInitializer(new ContractsManagementDbInitializer());
        }

        public virtual DbSet<Entities.Contract> Contracts { get; set; }
        public virtual DbSet<Entities.MinimumSalary> MinimumSalaries { get; set; }
        public virtual DbSet<Entities.ContractType> ContractTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
