﻿using ContractsManagement.Repository.Calculators;
using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository.Factories
{
    public interface ISalaryCalculatorFactory
    {
        ISalaryCalculator GetSalaryCalculator(ContractType contractType);
    }
}