﻿using ContractsManagement.Repository.Entities;
using ContractsManagement.Repository.Calculators;

namespace ContractsManagement.Repository.Factories
{
    public class SalaryCalculatorFactory : ISalaryCalculatorFactory
    {
        public ISalaryCalculator GetSalaryCalculator(ContractType contractType)
        {
            ISalaryCalculator producedCalculator;

            switch (contractType.ContractTypeName)
            {
                case ContractTypes.Programmer:
                    producedCalculator = new ProgrammerSalaryCalculator(contractType);
                break;

                case ContractTypes.Tester:
                    producedCalculator = new TesterSalaryCalculator(contractType);
                break;

                default:
                    producedCalculator = null;
                break;
            }

            return producedCalculator;
        }
    }
}
