﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository.Factories
{
    public class ContractFactory : IContractFactory
    {
        public DTO.Contract GetContract(Contract contractEntity)
        {
            return new DTO.Contract {
                ContractId = contractEntity.ContractId,
                ContractName = contractEntity.ContractName,
                ContractTypeId = contractEntity.ContractTypeId,
                Salary = contractEntity.Salary,
                YearsOfExperience = contractEntity.YearsOfExperience
            };
        }
    }
}
