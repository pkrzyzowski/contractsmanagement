﻿using ContractsManagement.DTO;
using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository.Factories
{
    public interface IContractFactory
    {
        DTO.Contract GetContract(Entities.Contract contractEntity);
    }
}