﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository
{

    public interface IContractsManagementRepository
    {
        IQueryable<ContractType> GetContractTypes();
        IQueryable<Contract> GetContracts();
        Contract GetContract(int contractId);
        RepositoryStatus<Contract> CreateContract(DTO.Contract contract, bool onlyValidation = false);

        RepositoryStatus<Contract> InsertContract(Contract contract);
        RepositoryStatus<Contract> UpdateContract(int contractId, Contract contract);
        RepositoryStatus<Contract> DeleteContract(int contractId);
        decimal CalculateSalary(int contractTypeId, int yearsOfExperience);  
    }
}
