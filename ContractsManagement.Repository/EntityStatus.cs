﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Repository
{
    public enum EntityStatus
    {
        Created,
        NotFound,
        Deleted,
        NothingModified,
        Error,
        Updated,
        Selected
    }
}
