﻿using System;
using System.Linq;
using System.Data.Entity;

using ContractsManagement.Repository.Entities;
using ContractsManagement.Repository.Calculators;
using ContractsManagement.Repository.Factories;
using System.Collections.Generic;

namespace ContractsManagement.Repository
{
    public class ContractsManagementRepository : IContractsManagementRepository, IDisposable
    {
        private ContractsManagementContext context;
        private ISalaryCalculatorFactory salaryCalculatorFactory;
        private bool disposed = false;

        private decimal? GetMinimumSalary(IEnumerable<MinimumSalary> minimumSalaries, int yearsOfExperience)
        {
            // If we're unable to find minimum salary value, we can assume that configuration is broken...
            return minimumSalaries.SingleOrDefault(
                p => p.LowerLimitOfExperience <= yearsOfExperience
                && p.UpperLimitOfExperience >= yearsOfExperience
            )?.Salary;
        }

        public ContractsManagementRepository(ContractsManagementContext dbContext, ISalaryCalculatorFactory salaryCalculatorFactory)
        {
            this.context = dbContext;
            this.salaryCalculatorFactory = salaryCalculatorFactory;
        }

        public ContractsManagementRepository() : this(new ContractsManagementContext(), new SalaryCalculatorFactory())
        {
        }

        public IQueryable<ContractType> GetContractTypes()
        {
            return this.context.ContractTypes;
        }

        public IQueryable<Contract> GetContracts()
        {
            return this.context.Contracts;
        }

        public Contract GetContract(int contractId)
        {
            return this.context.Contracts.SingleOrDefault(p => p.ContractId == contractId);
        }

        public RepositoryStatus<Contract> CreateContract(DTO.Contract contract, bool onlyValidation = false)
        {
            List<ValidationError> validationErrors = new List<ValidationError>();

            if (contract == null) validationErrors.Add(new ValidationError { FieldName = "*", Message = "Input DTO is null!" });
            if (string.IsNullOrWhiteSpace(contract.ContractName)) validationErrors.Add(new ValidationError { FieldName = "ContractName", Message = "Contract name cannot be null." });

            ContractType contractType = context.ContractTypes.SingleOrDefault(p => p.ContractTypeId == contract.ContractTypeId);
            if (contractType == null)
            {
                //Without correct contract type we can't say if years of experience match to company policy. 
                //So the only way we can adhere to this situation is to return all the errors and say the DTO is broken.
                validationErrors.Add(new ValidationError { FieldName = "ContractTypeId", Message = "Unknown contract type." });
                return new RepositoryStatus<Contract>(null, EntityStatus.Error, validationErrors);
            }

            decimal? minimumSalary = GetMinimumSalary(contractType.MinimumSalaries, contract.YearsOfExperience);
            if(minimumSalary == null)
                validationErrors.Add(new ValidationError { FieldName = "YearsOfExperience", Message = "Contract years of experience doesn't match the company salaries policy." });

            //At this point we can say that data we get are not valid, and there is no sense to contine processing.
            if (validationErrors.Any()) return new RepositoryStatus<Contract>(null, EntityStatus.Error, validationErrors);

            decimal calculatedSalary = this.salaryCalculatorFactory.GetSalaryCalculator(
                contractType
            ).CalculateSalary((decimal)minimumSalary, contract.YearsOfExperience);

            if (onlyValidation)
            {
                return new RepositoryStatus<Contract>(null, EntityStatus.Created);
            }
            else
            {
                var createdContract = new Contract
                {
                    ContractName = contract.ContractName,
                    ContractTypeId = contract.ContractTypeId,
                    YearsOfExperience = contract.YearsOfExperience,
                    Salary = calculatedSalary
                };

                return new RepositoryStatus<Contract>(createdContract, EntityStatus.Created);
            }
        }

        public RepositoryStatus<Contract> DeleteContract(int contractId)
        {
            try
            {
                var contract = this.context.Contracts.SingleOrDefault(p => p.ContractId == contractId);
                if (contract == null) return new RepositoryStatus<Contract>(null, EntityStatus.NotFound);

                this.context.Contracts.Remove(contract);
                this.context.SaveChanges();
                return new RepositoryStatus<Contract>(null, EntityStatus.Deleted);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.Write(e.ToString());
                return new RepositoryStatus<Contract>(null, EntityStatus.Error);
            }
        }

        public RepositoryStatus<Contract> InsertContract(Contract contract)
        {
            try
            {
                this.context.Contracts.Add(contract);
                int rowsAffected = this.context.SaveChanges();

                if(rowsAffected > 0)
                {
                    return new RepositoryStatus<Contract>(contract, EntityStatus.Created);
                }
                else
                {
                    return new RepositoryStatus<Contract>(null, EntityStatus.NothingModified);
                }

            } catch(Exception e)
            {
                System.Diagnostics.Debug.Write(e.ToString());
                return new RepositoryStatus<Contract>(null, EntityStatus.Error);
            }
        }

        public RepositoryStatus<Contract> UpdateContract(int contractId, Contract contract)
        {
            try
            {
                var existingContract = this.context.Contracts.SingleOrDefault(p => p.ContractId == contractId);
                if (existingContract == null) return new RepositoryStatus<Contract>(null, EntityStatus.NotFound);

                existingContract.ContractName = contract.ContractName;
                existingContract.ContractTypeId = contract.ContractTypeId;
                existingContract.Salary = contract.Salary;
                existingContract.YearsOfExperience = contract.YearsOfExperience;
                
                int rowsAffected = this.context.SaveChanges();
                if (rowsAffected > 0) return new RepositoryStatus<Contract>(existingContract, EntityStatus.Updated);
                else return new RepositoryStatus<Contract>(existingContract, EntityStatus.NothingModified);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.Write(e.ToString());
                return new RepositoryStatus<Contract>(null, EntityStatus.Error);
            }
        }
        
        public decimal CalculateSalary(int contractTypeId, int yearsOfExperience)
        {
            decimal minimumSalary = (decimal)GetMinimumSalary(this.context.MinimumSalaries.Where(p => p.ContractTypeId == contractTypeId), yearsOfExperience);
            ISalaryCalculator salaryCalculator = this.salaryCalculatorFactory.GetSalaryCalculator(this.context.ContractTypes.Single(p => p.ContractTypeId == contractTypeId));

            return salaryCalculator.CalculateSalary(minimumSalary, yearsOfExperience);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if(disposing)
            {
                this.context.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
