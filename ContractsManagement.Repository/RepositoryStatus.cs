﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Repository
{
    public class RepositoryStatus<T> where T : class
    {
        public T Entity { get; private set; }
        public EntityStatus Status { get; private set; }
        public List<ValidationError> ValidationErrors { get; private set; }

        public RepositoryStatus(T entity, EntityStatus status, List<ValidationError> validationErrors = null)
        {
            this.Entity = entity;
            this.Status = status;
            this.ValidationErrors = validationErrors;
        }
    }
}
