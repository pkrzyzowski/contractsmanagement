﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ContractsManagement.Repository.Entities;

namespace ContractsManagement.Repository.Calculators
{
    public class ProgrammerSalaryCalculator : ISalaryCalculator
    {
        private ContractType contractType;
        public ProgrammerSalaryCalculator(ContractType contractType)
        {
            this.contractType = contractType;
        }

        public decimal CalculateSalary(decimal MinimumSalary, int YearsOfExperience)
        {
            return MinimumSalary + (YearsOfExperience * contractType.MultiplyFactor);
        }
    }
}
