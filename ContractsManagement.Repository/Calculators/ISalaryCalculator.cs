﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Repository.Calculators
{
    public interface ISalaryCalculator
    {
        decimal CalculateSalary(decimal MinimumSalary, int YearsOfExperience);
    }
}
