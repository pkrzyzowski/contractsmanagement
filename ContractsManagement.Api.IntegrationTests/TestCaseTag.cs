﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContractsManagement.Api.IntegrationTests
{
    public class TestCaseTag
    {
        public static string Uri = "Uri";
        public static string Method = "Method";
        public static string RequestBody = "RequestBody";
        public static string ExpectedResponseBody = "ExpectedResponseBody";
        public static string ExpectedStatusCode = "ExpectedStatusCode";
        public static string ExpectedPaginationHeader = "ExpectedPaginationHeader";
        public static string RequestDTO = "RequestDTO";
    }
}
