﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Owin.Testing;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;

using ContractsManagement.Api.Headers;

namespace ContractsManagement.Api.IntegrationTests
{
    [TestClass]
    public class HappyPath : IDisposable
    {
        private bool disposed = false;
        private TestServer testServer;
        private TestContext testContext;
        private static string DataDirectory = Directory.GetCurrentDirectory();

        public TestContext TestContext
        {
            get { return testContext; }
            set { testContext = value; }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", DataDirectory);
            this.testServer = TestServer.Create<Startup>();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            Dispose();
        }

        private object ReadTestCaseSetting(string settingName)
        {
            if (this.testContext.DataRow.Table.Columns.Contains(settingName))
                return this.testContext.DataRow[settingName];
            else return null;
        }

        private object DeserializeBody(Type objectType, string body)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject(body, objectType);
        }

        private static Type GetType(string TypeName)
        {
            return AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .FirstOrDefault(t => t.FullName.StartsWith(TypeName));
        }

        private HttpResponseMessage SendRequest(string url, string method, string requestBody = null, string requestDTOType = null)
        {
            RequestBuilder request = this.testServer.CreateRequest(url);
            if (!string.IsNullOrWhiteSpace(requestBody))
            {
                request.And(r => r.Content = new ObjectContent(GetType(requestDTOType), DeserializeBody(GetType(requestDTOType), requestBody), new JsonMediaTypeFormatter()));
            }

            Task<HttpResponseMessage> response = request.SendAsync(method);
            response.Wait();

            return response.Result;
        }
        
        private bool CompareContracts(DTO.Contract expectedContract, DTO.Contract actualContract)
        {
            return
                    expectedContract.ContractName == actualContract.ContractName
                    && expectedContract.ContractTypeId == actualContract.ContractTypeId
                    && expectedContract.Salary == actualContract.Salary
                    && expectedContract.YearsOfExperience == actualContract.YearsOfExperience;
        }

        private bool IsResponseBodyValid(string dtoType, string expected, string actual)
        {
            switch(dtoType)
            {
                case "ContractsManagement.DTO.Contract":
                    {
                        DTO.Contract expectedContract = (DTO.Contract)DeserializeBody(typeof(DTO.Contract), expected);
                        DTO.Contract actualContract = (DTO.Contract)DeserializeBody(typeof(DTO.Contract), actual);
                        return CompareContracts(expectedContract, actualContract);
                    }
                case "ContractsManagement.DTO.Contract[]":
                    {
                        DTO.Contract[] expectedContracts = (DTO.Contract[])DeserializeBody(typeof(DTO.Contract[]), expected);
                        DTO.Contract[] actualContracts = (DTO.Contract[])DeserializeBody(typeof(DTO.Contract[]), actual);

                        bool areContractsEqual = true;
                        //I assumed that person who prepared integration test scenario, prepared expected array in correct order:
                        for (int i = 0; i < expectedContracts.Length; i++)
                        {
                            areContractsEqual &= CompareContracts(expectedContracts[i], actualContracts[i]);
                        }
                        Assert.IsTrue(areContractsEqual);
                        return areContractsEqual;
                    }
                case "Decimal":
                    {
                        decimal expectedValue = decimal.Parse(expected);
                        decimal actualValue = decimal.Parse(expected);
                        return expectedValue == actualValue;
                    }
                default:
                    return expected == actual;
            }
        }

        [TestMethod, TestCategory("IntegrationTests")]
        [DeploymentItem("TestCases\\HappyPath.xml")]
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.XML", "HappyPath.xml", "Request", DataAccessMethod.Sequential)]
        public void IntegrationTests_HappyPath()
        {
            //Arrange:
            string url = ReadTestCaseSetting(TestCaseTag.Uri)?.ToString();
            string method = ReadTestCaseSetting(TestCaseTag.Method)?.ToString();
            string requestBody = ReadTestCaseSetting(TestCaseTag.RequestBody)?.ToString();
            string requestDTOType = ReadTestCaseSetting(TestCaseTag.RequestDTO)?.ToString();
            string expectedResponseBody = ReadTestCaseSetting(TestCaseTag.ExpectedResponseBody)?.ToString();
            int expectedStatusCode = int.Parse(ReadTestCaseSetting(TestCaseTag.ExpectedStatusCode).ToString());
            string expectedPaginationHeader = ReadTestCaseSetting(TestCaseTag.ExpectedPaginationHeader)?.ToString();
            bool validateResponseBody = !string.IsNullOrWhiteSpace(expectedResponseBody);

            //Act:
            HttpResponseMessage response = SendRequest(url, method, requestBody, requestDTOType);

            //Assert:
            Assert.IsNotNull(response);
            Assert.AreEqual(expectedStatusCode, (int)response.StatusCode);

            if (validateResponseBody)
            {
                Assert.IsTrue(IsResponseBodyValid(requestDTOType, expectedResponseBody, response.Content.ReadAsStringAsync().Result));
            }
            if(!string.IsNullOrWhiteSpace(expectedPaginationHeader))
            {
                Assert.IsTrue(response.Headers.Contains(PaginationHeader.PaginationHeaderName));
                Assert.AreEqual(PaginationHeader.Parse(expectedPaginationHeader), PaginationHeader.Parse(response.Headers.Single(p => p.Key == PaginationHeader.PaginationHeaderName).Value.First()));
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.disposed)
                return;
            if (disposing)
            {
                this.testServer.Dispose();
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
