﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq.Expressions;
using ContractsManagement.Repository;
using ContractsManagement.Repository.Entities;
using ContractsManagement.Repository.Factories;

using System.Web.Http.Routing;
using System.Web;

namespace ContractsManagement.Api.Controllers
{
    public class ContractController : ApiController
    {
        private IContractsManagementRepository repository;
        public const int maxPageSize = 10;
        private ContractFactory contractFactory;

        public ContractController(IContractsManagementRepository repository)
        {
            this.repository = repository;
            contractFactory = new ContractFactory();
        }

        private string SerializeObjectToJson(object obj)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(obj);
        }
        
        [HttpGet]
        public IHttpActionResult Get(int pageNumber = 1, int pageSize = maxPageSize, string filtering = null)
        {
            try
            {
                var contracts = repository.GetContracts();
                if (!string.IsNullOrWhiteSpace(filtering))
                {
                    foreach(string column in filtering.Split(';'))
                    {
                        string[] filteringOption = column.Split(',');
                        if (filteringOption.Length != 2) return BadRequest();

                        switch (filteringOption[0].ToLower())
                        {
                            case "salary":
                                {
                                    decimal value = decimal.Parse(filteringOption[1]);
                                    contracts = contracts.Where(p => p.Salary > value);
                                }
                                break;
                            case "experience":
                                {
                                    int value = int.Parse(filteringOption[1]);
                                    contracts = contracts.Where(p => p.YearsOfExperience > value);
                                }
                                break;
                            case "name":
                                {
                                    string value = filteringOption[1];
                                    contracts = contracts.Where(p => p.ContractName.Contains(value));
                                }
                                break;
                            case "type":
                                {
                                    int value = int.Parse(filteringOption[1]);
                                    contracts = contracts.Where(p => p.ContractTypeId == value);
                                }
                                break;
                        }
                    }
                }

                if (contracts == null) return NotFound();

                if (pageSize > maxPageSize) pageSize = maxPageSize;

                var contractsCount = contracts.Count();
                var pagesCount = (int)Math.Ceiling((float)contractsCount / pageSize);
                UrlHelper urlHelper = new UrlHelper(Request);

                var paginationHeader = SerializeObjectToJson(
                    new Headers.PaginationHeader
                    {
                        CurrentPage = pageNumber,
                        PageSize = pageSize,
                        PagesCount = pagesCount,
                        PreviousPage = pageNumber > 1 ? urlHelper.Link("All Contracts", new { pageNumber = pageNumber - 1, pageSize = pageSize }) : string.Empty,
                        NextPage = pageNumber < pagesCount ? urlHelper.Link("All Contracts", new { pageNumber = pageNumber + 1, pageSize = pageSize }) : string.Empty,
                    }
                );

                Request.GetOwinContext().Response.Headers.Append(Headers.PaginationHeader.PaginationHeaderName, paginationHeader);

                return Ok(
                    contracts
                        .OrderBy(p=>p.ContractId)
                        .Skip(pageSize * (pageNumber - 1))
                        .Take(pageSize)
                        .ToList()
                        .Select(p=>this.contractFactory.GetContract(p))
                );
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var contract = this.repository.GetContract(id);
                if (contract == null) return NotFound();

                return Ok(this.contractFactory.GetContract(contract));
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]DTO.Contract contract)
        {
            var newContract = this.repository.CreateContract(contract);
            if(newContract.Status == EntityStatus.Error)
            {
                return BadRequest();
            }

            try
            {
                var insertedContract = this.repository.InsertContract(newContract.Entity);
                return Ok(this.contractFactory.GetContract(insertedContract.Entity));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPut]
        public IHttpActionResult Put(int id, [FromBody]DTO.Contract contract)
        {
            var updatedContract = this.repository.CreateContract(contract);
            if (updatedContract.Status == EntityStatus.Error)
            {
                return BadRequest();
            }

            try
            {
                var updateResult = this.repository.UpdateContract(id, updatedContract.Entity);
                if (updateResult.Status == EntityStatus.NotFound) return NotFound();
                if (updateResult.Status == EntityStatus.NothingModified) return StatusCode(HttpStatusCode.NotModified);
                if(updateResult.Status == EntityStatus.Error) return InternalServerError();

                return Ok(this.contractFactory.GetContract(updateResult.Entity));
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var result = this.repository.DeleteContract(id);
                if (result.Status == EntityStatus.Deleted) return StatusCode(HttpStatusCode.NoContent);
                else if (result.Status == EntityStatus.NotFound) return NotFound();

                return BadRequest();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
