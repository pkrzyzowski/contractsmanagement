﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ContractsManagement.Repository;
using ContractsManagement.Repository.Factories;

namespace ContractsManagement.Api.Controllers
{
    public class ContractValidationController : ApiController
    {
        private IContractsManagementRepository repository;
        private ContractFactory contractFactory;

        public ContractValidationController(IContractsManagementRepository repository)
        {
            this.repository = repository;
            contractFactory = new ContractFactory();
        }

        [HttpPost]
        public IHttpActionResult Post([FromBody]DTO.Contract contract)
        {
            var validationResult = this.repository.CreateContract(contract, true);
            if (validationResult.ValidationErrors == null)
            {
                return Ok();
            } else
            {
                return BadRequest(Newtonsoft.Json.JsonConvert.SerializeObject(validationResult.ValidationErrors));
            }
        }
    }
}
