﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ContractsManagement.Repository;
using ContractsManagement.Repository.Calculators;

namespace ContractsManagement.Api.Controllers
{
    [RoutePrefix(Constans.RoutePrefix)]
    public class SalaryController : ApiController
    {
        private IContractsManagementRepository repository;

        public SalaryController(IContractsManagementRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("salary/{contractTypeId}/{yearsOfExperience}")]
        public IHttpActionResult Get(int contractTypeId, int yearsOfExperience)
        {
            try {
                return Ok(this.repository.CalculateSalary(contractTypeId, yearsOfExperience));
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
