﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ContractsManagement.Repository;


namespace ContractsManagement.Api.Controllers
{
    public class ContractTypesController : ApiController
    {
        private IContractsManagementRepository repository;

        public ContractTypesController(IContractsManagementRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(repository.GetContractTypes().Select(p => new DTO.ContractType { ContractTypeId = p.ContractTypeId, ContractTypeName = p.ContractTypeName }));
            }
            catch(Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
