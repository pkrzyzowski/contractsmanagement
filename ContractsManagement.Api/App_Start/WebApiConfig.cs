﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace ContractsManagement.Api
{
    public static class WebApiConfig
    {

        public static HttpConfiguration RegisterRoutes(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "All Contracts",
                routeTemplate: Constans.RoutePrefix + "/contracts/{pageNumber}/{pageSize}/{filtering}",
                defaults: new { controller = "contract", pageNumber = RouteParameter.Optional, pageSize = RouteParameter.Optional, filtering = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: Constans.RoutePrefix + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            return config;
        }
    }
}
