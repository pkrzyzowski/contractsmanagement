﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ContractsManagement.Api.Headers
{
    public class PaginationHeader
    {
        public const string PaginationHeaderName = "PaginationHeader";

        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int PagesCount { get; set; }
        public string PreviousPage { get; set; }
        public string NextPage { get; set; }

        public static PaginationHeader Parse(string serializedPaginationHeader)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<PaginationHeader>(serializedPaginationHeader);
        }

        public override bool Equals(object obj)
        {
            PaginationHeader otherHeader = (PaginationHeader)obj;
            return
                this.CurrentPage == otherHeader.CurrentPage
                && this.PageSize == otherHeader.PageSize
                && this.PagesCount == otherHeader.PagesCount
                && this.PreviousPage == otherHeader.PreviousPage
                && this.NextPage == otherHeader.NextPage;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}