﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Autofac;
using System.Web.Http;
using Autofac.Integration.WebApi;
using System.Reflection;

using ContractsManagement.Repository;
using System.Web.Compilation;

[assembly: OwinStartup(typeof(ContractsManagement.Api.Startup))]

namespace ContractsManagement.Api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            config = WebApiConfig.RegisterRoutes(config);

            var builder = new ContainerBuilder();

            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterApiControllers(currentAssembly).InstancePerRequest();

            builder.RegisterType<ContractsManagementRepository>().As<IContractsManagementRepository>();

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacWebApi(config);

            app.UseWebApi(config);
        }
    }
}
