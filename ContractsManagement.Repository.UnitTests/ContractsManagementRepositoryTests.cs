﻿using Moq;
using Ploeh.AutoFixture;
using System.Linq;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ContractsManagement.Repository;
using ContractsManagement.Repository.Factories;
using ContractsManagement.Repository.Entities;
using ContractsManagement.Repository.Calculators;
using System.Collections.Generic;

namespace ContractsManagement.Repository.UnitTests
{
    [TestClass]
    public class ContractsManagementRepositoryTests
    {
        private const decimal salary = 5000;

        private Fixture fixture;
        private Mock<ISalaryCalculator> salaryCalculator; 
        private Mock<ISalaryCalculatorFactory> salaryCalculatorFactory;
        private Mock<ContractsManagementContext> dbContext;
        private ContractsManagementRepository repository;

        private IQueryable<Contract> CreateContractsList(int numberOfEntities)
        {
            return this.fixture.CreateMany<Contract>(numberOfEntities).AsQueryable();
        }

        private Mock<DbSet<Contract>> SetupContractsDbSet(IQueryable<Contract> testContractsData)
        {
            Mock<DbSet<Contract>> testContractsDbSet = new Mock<DbSet<Contract>>();
            testContractsDbSet.As<IQueryable<Contract>>().Setup(p => p.Provider).Returns(testContractsData.Provider);
            testContractsDbSet.As<IQueryable<Contract>>().Setup(p => p.Expression).Returns(testContractsData.Expression);
            testContractsDbSet.As<IQueryable<Contract>>().Setup(p => p.ElementType).Returns(testContractsData.ElementType);
            testContractsDbSet.As<IQueryable<Contract>>().Setup(p => p.GetEnumerator()).Returns(testContractsData.GetEnumerator());

            this.dbContext.Setup(p => p.Contracts).Returns(testContractsDbSet.Object);

            return testContractsDbSet;
        }

        private void SetupContractTypesDbSet(IQueryable<ContractType> testContractTypesData)
        {
            Mock<DbSet<ContractType>> testContractTypesDbSet = new Mock<DbSet<ContractType>>();
            testContractTypesDbSet.As<IQueryable<ContractType>>().Setup(p => p.Provider).Returns(testContractTypesData.Provider);
            testContractTypesDbSet.As<IQueryable<ContractType>>().Setup(p => p.Expression).Returns(testContractTypesData.Expression);
            testContractTypesDbSet.As<IQueryable<ContractType>>().Setup(p => p.ElementType).Returns(testContractTypesData.ElementType);
            testContractTypesDbSet.As<IQueryable<ContractType>>().Setup(p => p.GetEnumerator()).Returns(testContractTypesData.GetEnumerator());

            this.dbContext.Setup(p => p.ContractTypes).Returns(testContractTypesDbSet.Object);
        }

        private Mock<DbSet<Contract>> SetupEmptyContractList()
        {
            return SetupContractsDbSet(new List<Contract>().AsQueryable());
        }


        [TestInitialize]
        public void TestInitialize()
        {
            this.fixture = new Fixture();

            this.salaryCalculator = new Mock<ISalaryCalculator>();
            this.salaryCalculator.Setup(p => p.CalculateSalary(It.IsAny<decimal>(), It.IsAny<int>())).Returns(salary);

            this.salaryCalculatorFactory = new Mock<ISalaryCalculatorFactory>();
            this.salaryCalculatorFactory.Setup(p => p.GetSalaryCalculator(It.IsAny<ContractType>())).Returns(this.salaryCalculator.Object);

            this.dbContext = new Mock<ContractsManagementContext>();
            
            this.repository = new ContractsManagementRepository(this.dbContext.Object, this.salaryCalculatorFactory.Object);
        }

        [TestMethod]
        public void GetContractTypesShouldReturnAllContractTypes()
        {
            //Arrange:
            IQueryable<ContractType> testContractTypesData = this.fixture.CreateMany<ContractType>(2).AsQueryable();
            SetupContractTypesDbSet(testContractTypesData);

            //Act:
            IQueryable<ContractType> result = this.repository.GetContractTypes();

            //Assert:
            for(int i = 0; i < testContractTypesData.Count(); i++)
            {
                Assert.AreEqual(testContractTypesData.ElementAt(i), result.ElementAt(i));
            }
        }

        [TestMethod]
        public void GetContractsShouldReturnAllContractTypes()
        {
            //Arrange:
            IQueryable<Contract> testContractsData = CreateContractsList(10);
            SetupContractsDbSet(testContractsData);

            //Act:
            IQueryable<Contract> result = this.repository.GetContracts();

            //Assert:
            for (int i = 0; i < testContractsData.Count(); i++)
            {
                Assert.AreEqual(testContractsData.ElementAt(i), result.ElementAt(i));
            }
        }

        [TestMethod]
        public void GetContractShoulReturnCorrectEntity()
        {
            //Arrange:
            IQueryable<Contract> testContractsData = CreateContractsList(10);
            SetupContractsDbSet(testContractsData);

            //Act:
            Contract result = this.repository.GetContract(
                testContractsData.ElementAt(5).ContractId
            );

            //Assert:
            Assert.AreEqual(testContractsData.ElementAt(5), result);
        }
        
        private IQueryable<ContractType> CreateTestContractType()
        {
            return new List<ContractType>
            {
                new ContractType { ContractTypeId = 1, ContractTypeName = "test", MinimumSalaries = new List<MinimumSalary> { new MinimumSalary { ContractTypeId = 1, LowerLimitOfExperience = 1, Salary = 100, UpperLimitOfExperience = int.MaxValue } }, MultiplyFactor = 423 }
            }.AsQueryable();
        }

        [TestMethod]
        public void CreateContractShouldReturnValidationErrorsOnInvalidContract()
        {
            //Arrange:
            IQueryable<ContractType> testContractTypes = CreateTestContractType();
            SetupContractTypesDbSet(testContractTypes);
            
            DTO.Contract testContract = new DTO.Contract { ContractName = "test", ContractTypeId = 10, YearsOfExperience = 14 };

            //Act:
            RepositoryStatus<Contract> result = this.repository.CreateContract(testContract);

            //Assert:
            Assert.IsNull(result.Entity);
            Assert.AreEqual(EntityStatus.Error, result.Status);
            Assert.IsTrue(result.ValidationErrors.Any(p => p.FieldName == "ContractTypeId"));
        }

        [TestMethod]
        public void CreateContractShouldReturnCreatedContract()
        {
            //Arrange:
            IQueryable<ContractType> testContractTypes = CreateTestContractType();
            SetupContractTypesDbSet(testContractTypes);

            DTO.Contract testContract = new DTO.Contract { ContractName = "test", ContractTypeId = 1, YearsOfExperience = 14 };

            //Act:
            RepositoryStatus<Contract> result = this.repository.CreateContract(testContract);

            //Assert:
            Assert.IsNotNull(result.Entity);
            Assert.AreEqual(result.Status, EntityStatus.Created);
            Assert.IsNull(result.ValidationErrors);
            Assert.AreEqual(salary, result.Entity.Salary);
        }

        [TestMethod]
        public void CreateContractShouldReturnOnlyValidationStatus()
        {
            //Arrange:
            IQueryable<ContractType> testContractTypes = CreateTestContractType();
            SetupContractTypesDbSet(testContractTypes);

            DTO.Contract testContract = new DTO.Contract { ContractName = "test", ContractTypeId = 1, YearsOfExperience = 14 };

            //Act:
            RepositoryStatus<Contract> result = this.repository.CreateContract(testContract, true);

            //Assert:
            Assert.IsNull(result.Entity);
            Assert.AreEqual(EntityStatus.Created, result.Status);
            Assert.IsNull(result.ValidationErrors);
        }

        [TestMethod]
        public void DeleteContractShouldReturnNotFoundStatus()
        {
            //Assert:
            SetupContractTypesDbSet(CreateTestContractType());

            IQueryable<Contract> testContractList = new List<Contract> {
                new Contract { ContractId = 1, ContractName = "test", ContractTypeId = 1, Salary = 5050, YearsOfExperience = 5 }
            }.AsQueryable();
            SetupContractsDbSet(testContractList);

            //Act:
            RepositoryStatus<Contract> result = this.repository.DeleteContract(15);

            //Assert:
            Assert.AreEqual(EntityStatus.NotFound, result.Status);
        }

        [TestMethod]
        public void DeleteContractShouldRemoveContract()
        {
            //Assert:
            SetupContractTypesDbSet(CreateTestContractType());

            IQueryable<Contract> testContractList = new List<Contract> {
                new Contract { ContractId = 1, ContractName = "test", ContractTypeId = 1, Salary = 5050, YearsOfExperience = 5 }
            }.AsQueryable();
            var testContractsDbSet = SetupContractsDbSet(testContractList);

            //Act:
            RepositoryStatus<Contract> result = this.repository.DeleteContract(1);

            //Assert:

            testContractsDbSet.Verify(p => p.Remove(It.IsAny<Contract>()), Times.Once);
            this.dbContext.Verify(p => p.SaveChanges(), Times.Once);
            Assert.AreEqual(EntityStatus.Deleted, result.Status);
        }
        
        [TestMethod]
        public void InsertContractShouldReturnCreatedStatus()
        {
            //Assert:
            var testContractsDbSet = SetupEmptyContractList();
            this.dbContext.Setup(p => p.SaveChanges()).Returns(1);

            //Act:
            RepositoryStatus<Contract> result = this.repository.InsertContract(this.fixture.Create<Contract>());

            //Assert:
            testContractsDbSet.Verify(p => p.Add(It.IsAny<Contract>()), Times.Once);
            this.dbContext.Verify(p => p.SaveChanges(), Times.Once);
            Assert.AreEqual(EntityStatus.Created, result.Status);
            Assert.IsNotNull(result.Entity);
        }

        [TestMethod]
        public void InsertContractShouldReturnNothingModifiedStatus()
        {
            //Assert:
            var testContractsDbSet = SetupEmptyContractList();
            this.dbContext.Setup(p => p.SaveChanges()).Returns(0);

            //Act:
            RepositoryStatus<Contract> result = this.repository.InsertContract(this.fixture.Create<Contract>());

            //Assert:
            testContractsDbSet.Verify(p => p.Add(It.IsAny<Contract>()), Times.Once);
            this.dbContext.Verify(p => p.SaveChanges(), Times.Once);
            Assert.AreEqual(EntityStatus.NothingModified, result.Status);
            Assert.IsNull(result.Entity);
        }

        [TestMethod]
        public void UpdateContractShouldReturnNotFoundStatus()
        {
            //Assert:
            var testContractsDbSet = SetupEmptyContractList();

            //Act:
            RepositoryStatus<Contract> result = this.repository.UpdateContract(15, fixture.Create<Contract>());

            //Assert:
            Assert.AreEqual(EntityStatus.NotFound, result.Status);
        }

        [TestMethod]
        public void UpdateContractShouldReturnNothingModifiedStatus()
        {
            //Assert:
            SetupContractTypesDbSet(CreateTestContractType());

            IQueryable<Contract> testContractList = new List<Contract> {
                new Contract { ContractId = 1, ContractName = "test", ContractTypeId = 1, Salary = 5050, YearsOfExperience = 5 }
            }.AsQueryable();
            SetupContractsDbSet(testContractList);
            this.dbContext.Setup(p => p.SaveChanges()).Returns(0);

            //Act:
            RepositoryStatus<Contract> result = this.repository.UpdateContract(1, fixture.Create<Contract>());

            //Assert:
            Assert.AreEqual(EntityStatus.NothingModified, result.Status);
        }

        [TestMethod]
        public void UpdateContractShouldReturnUpdatedStatus()
        {
            //Assert:
            SetupContractTypesDbSet(CreateTestContractType());

            IQueryable<Contract> testContractList = new List<Contract> {
                new Contract { ContractId = 1, ContractName = "test", ContractTypeId = 1, Salary = 5050, YearsOfExperience = 5 }
            }.AsQueryable();
            SetupContractsDbSet(testContractList);
            this.dbContext.Setup(p => p.SaveChanges()).Returns(1);

            //Act:
            RepositoryStatus<Contract> result = this.repository.UpdateContract(1, fixture.Create<Contract>());

            //Assert:
            Assert.AreEqual(EntityStatus.Updated, result.Status);
        }

        [TestMethod]
        public void CalculateSalaryShouldReturnSalaryForProvidedType()
        {
            //Arrange:
            int testContractType = 1;
            int testYearsOfExperience = 4;
            decimal testMinimumSalary = 100;

            SetupContractTypesDbSet(CreateTestContractType());
            IQueryable<MinimumSalary> testMinimumSalariesData = new List<MinimumSalary>
            {
                new MinimumSalary { ContractTypeId = 1, LowerLimitOfExperience = int.MinValue, UpperLimitOfExperience = int.MaxValue, Salary = testMinimumSalary }
            }.AsQueryable();

            Mock<DbSet<MinimumSalary>> testMinimumSalariesDbSet = new Mock<DbSet<MinimumSalary>>();
            testMinimumSalariesDbSet.As<IQueryable<MinimumSalary>>().Setup(p => p.Provider).Returns(testMinimumSalariesData.Provider);
            testMinimumSalariesDbSet.As<IQueryable<MinimumSalary>>().Setup(p => p.Expression).Returns(testMinimumSalariesData.Expression);
            testMinimumSalariesDbSet.As<IQueryable<MinimumSalary>>().Setup(p => p.ElementType).Returns(testMinimumSalariesData.ElementType);
            testMinimumSalariesDbSet.As<IQueryable<MinimumSalary>>().Setup(p => p.GetEnumerator()).Returns(testMinimumSalariesData.GetEnumerator());

            this.dbContext.Setup(p => p.MinimumSalaries).Returns(testMinimumSalariesDbSet.Object);

            //Act:
            var result = this.repository.CalculateSalary(testContractType, testYearsOfExperience);

            //Assert:
            Assert.AreEqual(salary, result);
            this.salaryCalculator.Verify(p => p.CalculateSalary(It.Is<decimal>(v =>v == testMinimumSalary), It.Is<int>(v => v == testYearsOfExperience)), Times.Once);
        }
    }
}
